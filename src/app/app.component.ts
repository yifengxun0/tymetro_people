import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'tarin';
  isButtonClicked = true;
  isButtonClicked2 = false;

  ordinaryTrainData =[
    {id: 1,trainName: '1',currentStation: "1站",nextStation:"2站",passengerCount: 66, position:6 },
    {id: 1,trainName: '1',currentStation: "2站",nextStation:"3站",passengerCount: 12, position:2.5 },
    {id: 1,trainName: '1',currentStation: "12站",nextStation:"13站",passengerCount: 90, position:12 },
  ];

  directTrainData =[
    {id: 1,trainName: '1',currentStation: "1站",nextStation:"2站",passengerCount: 66, position:1},
  ];
  constructor() { }

  ngOnInit(): void {
    this.animatePosition();
  }
  
  calculateTop(position: number) {
    const topValue = (position - 1) * 37 + 'px';
    return { top: topValue };
  };

  animatePosition() {
    let currentIndex = 0;
    const positions = [6, 6.5,7,7.5,8];
    const positions2 = [1,2,2.5,3,3.5];
    const positions3 = [12,13.5,1,4,6];
    const positions4 = [1,3,8,12,13];
    const intervalId = setInterval(() => {
      if (currentIndex < positions.length) {
        this.ordinaryTrainData[0].position = positions[currentIndex];
        this.ordinaryTrainData[1].position = positions2[currentIndex];
        this.ordinaryTrainData[2].position = positions3[currentIndex];
        this.directTrainData[0].position = positions4[currentIndex];
        currentIndex++;
      } else {
        currentIndex = 0;
      }
    }, 1000);
  }
  stations = [
    { number: 'A1', text: '台北車站',styleClass: 'directStation'},
    { number: 'A2', text: '台北車站',styleClass: 'ordinaryStation'},
    { number: 'A1', text: '台北車站',styleClass: 'directStation'},
    { number: 'A2', text: '台北車站',styleClass: 'ordinaryStation'},
    { number: 'A2', text: '台北車站',styleClass: 'ordinaryStation'},
    { number: 'A2', text: '台北車站',styleClass: 'ordinaryStation'},
    { number: 'A2', text: '台北車站',styleClass: 'ordinaryStation'},
    { number: 'A1', text: '台北車站',styleClass: 'directStation'},
    { number: 'A2', text: '台北車站',styleClass: 'ordinaryStation'},
    { number: 'A2', text: '台北車站',styleClass: 'ordinaryStation'},
    { number: 'A2', text: '台北車站',styleClass: 'ordinaryStation'},
    { number: 'A1', text: '台北車站',styleClass: 'directStation'},
    { number: 'A1', text: '台北車站',styleClass: 'directStation'},
    { number: 'A2', text: '台北車站',styleClass: 'ordinaryStation'},
    { number: 'A2', text: '台北車站',styleClass: 'ordinaryStation'},
    { number: 'A2', text: '台北車站',styleClass: 'ordinaryStation'},
    
    // 其他站點信息...
  ];

  active(buttonNumber: number) {
    if (buttonNumber === 1) {
      this.isButtonClicked = true;
      this.isButtonClicked2 = false;
    } else if (buttonNumber === 2) {
      this.isButtonClicked2 = true;
      this.isButtonClicked = false;
    }
  }
}
